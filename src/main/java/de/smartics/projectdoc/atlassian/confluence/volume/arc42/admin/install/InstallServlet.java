/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42.admin.install;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import de.smartics.projectdoc.atlassian.confluence.admin.AbstractAdminServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handles post-install and post-update messages for users.
 *
 * @see <a href="https://developer.atlassian.com/market/developing-for-the-marketplace/plugin-metadata-files-used-by-upm-and-marketplace">plugin-metadata-files-used-by-upm-and-marketplace</a>
 */
public class InstallServlet extends AbstractAdminServlet {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The class version identifier.
   */
  private static final long serialVersionUID = 1L;

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public InstallServlet(final UserManager userManager,
      final LoginUriProvider loginUriProvider,
      final TemplateRenderer renderer) {
    super(userManager, loginUriProvider, renderer);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected void executeGet(final HttpServletRequest request,
      final HttpServletResponse response) throws IOException, ServletException {
    executePost(request, response);
  }

  @Override
  protected void executePost(final HttpServletRequest request,
      final HttpServletResponse response) throws IOException, ServletException {
    final String requested = request.getParameter("install");
    if (StringUtils.isNotBlank(requested)) {
      renderResponse(response, requested);
    } else {
      renderResponse(response, "new");
    }
  }

  private void renderResponse(final HttpServletResponse response,
      final String requestType) throws IOException {
    response.setContentType("text/html;charset=utf-8");
    renderer.render(
        "de/smartics/projectdoc/volume/arc42/admin/install-" + requestType +
        ".vm", response.getWriter());
  }

  // --- object basics --------------------------------------------------------

}
