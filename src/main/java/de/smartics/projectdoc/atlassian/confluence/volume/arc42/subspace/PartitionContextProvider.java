/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42.subspace;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSpaceContextProvider;
import de.smartics.projectdoc.atlassian.confluence.page.partition.DoctypeBlueprintKeys;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PartitionContextProvider extends ProjectDocSpaceContextProvider {

  /**
   * The key to the blueprint that summarizes all subordinate blueprints.
   */
  public static final String DOCUMENT_MODULE_KEY =
      "de.smartics.atlassian.confluence" +
      ".smartics-projectdoc-confluence-space-arc42:projectdoc-subspace-arc42" +
      "-main-doctypehome-blueprint";

  /**
   * The names of templates of type doctypes.
   */
  private static final Set<String> TYPE_TEMPLATE_SHORT_KEYS;

  static {
    final Set<String> set = new HashSet<>();
    set.add("experience-level-home-template");
    set.add("module-type-home-template");
    set.add("resource-type-home-template");
    set.add("topic-type-home-template");

    set.add("architecture-decision-type-home-template");
    set.add("quality-home-template");
    set.add("requirement-type-home-template");
    set.add("technical-debt-type-home-template");
    set.add("view-type-home-template");
    TYPE_TEMPLATE_SHORT_KEYS = Collections.unmodifiableSet(set);
  }

  public PartitionContextProvider(final ContextProviderSupportService support) {
    super(support);
  }

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final BlueprintContext updatedBlueprintContext =
        super.updateBlueprintContext(blueprintContext);

    updatedBlueprintContext.put(DoctypeBlueprintKeys.DOCUMENTS_BLUEPRINT_KEY,
        DOCUMENT_MODULE_KEY);
    updatedBlueprintContext.put(
        DoctypeBlueprintKeys.ALTERNATIVE_HOMEPAGE_TEMPLATE_KEYS,
        TYPE_TEMPLATE_SHORT_KEYS);
    updatedBlueprintContext.put(
        DoctypeBlueprintKeys.ALTERNATIVE_HOMEPAGE_I18N_KEY,
        "projectdoc.doctype.type-home-template.name");

    return updatedBlueprintContext;
  }
}
