/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42.app.config;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.ContentBlueprintManager;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.TemplateRendererHelper;
import com.atlassian.confluence.plugins.createcontent.api.services.ContentBlueprintService;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class ConfluenceComponentImports {

  @Bean
  public ApplicationProperties applicationProperties() {
    return importOsgiService(ApplicationProperties.class);
  }

  @Bean
  public AttachmentManager attachmentManager() {
    return importOsgiService(AttachmentManager.class);
  }

  @Bean
  public I18nResolver i18nResolver() {
    return importOsgiService(I18nResolver.class);
  }

  @Bean
  public ContentBlueprintManager contentBlueprintManager() {
    return importOsgiService(ContentBlueprintManager.class);
  }

  @Bean
  public ContentBlueprintService contentBlueprintService() {
    return importOsgiService(ContentBlueprintService.class);
  }

  @Bean
  public TemplateRendererHelper templateRendererHelper() {
    return importOsgiService(TemplateRendererHelper.class);
  }

  @Bean
  public SpaceManager spaceManager() {
    return importOsgiService(SpaceManager.class);
  }

  @Bean
  public PageManager pageManager() {
    return importOsgiService(PageManager.class);
  }

  @Bean
  public LocaleManager localeManager() {
    return importOsgiService(LocaleManager.class);
  }

  @Bean
  public I18NBeanFactory i18NBeanFactory() {
    return importOsgiService(I18NBeanFactory.class);
  }

  @Bean
  public ApplicationContext applicationContext() {
    return importOsgiService(ApplicationContext.class);
  }

  //  <component-import
  //    key="renderer"
  //    name="Atlassian Velocity Template Renderer"
  //    interface="com.atlassian.templaterenderer.velocity.one.six
  //    .VelocityTemplateRenderer" />
  //  @Bean
  //  public TemplateRenderer renderer() {
  //    return importOsgiService(VelocityTemplateRenderer.class);
  //  }
  @Bean
  public TemplateRenderer templateRenderer() {
    return importOsgiService(TemplateRenderer.class);
  }

  @Bean
  public UserManager userManager() {
    return importOsgiService(UserManager.class);
  }

  @Bean
  public LoginUriProvider loginUriProvider() {
    return importOsgiService(LoginUriProvider.class);
  }
}
