/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.util.i18n.I18NBean;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.BlueprintContextNames;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.page.partition.DoctypeBlueprintKeys;

/**
 * Extends for injection.
 */
public class ProjectDocSwadContextProvider
    extends AbstractProjectDocContextProviderExt {
  /**
   * The name of the module with content references to be added as additional
   * documents.
   */
  private static final String SUBDOCUMENT_MODULE_KEY =
      "de.smartics.atlassian.confluence" +
      ".smartics-projectdoc-confluence-arc42" +
      ":projectdoc-blueprint-doctype-arc42-swad-section";

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocSwadContextProvider(
      final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {

    blueprintContext.put(DoctypeBlueprintKeys.SUBDOCUMENTS_BLUEPRINT_KEY,
        SUBDOCUMENT_MODULE_KEY);

    final I18NBean i18n = user.createI18n();
    final Object name = blueprintContext.get("projectdoc_doctype_volume_name");
    if (name == null) {
      final String altValue = i18n.getText("projectdoc.volume.swad.name");
      blueprintContext.put("projectdoc_doctype_volume_name", altValue);
      blueprintContext.put(BlueprintContextNames.NAME, altValue);
    } else {
      blueprintContext.put(BlueprintContextNames.NAME, name);
    }

    final Object shortDescription =
        blueprintContext.get("projectdoc_doctype_volume_shortDescription");
    if (shortDescription == null) {
      final String altValue =
          i18n.getText("projectdoc.volume.swad.shortDescription");
      blueprintContext.put("projectdoc_doctype_volume_shortDescription",
          altValue);
      blueprintContext.put(BlueprintContextNames.SHORT_DESCRIPTION, altValue);
    } else {
      blueprintContext.put(BlueprintContextNames.SHORT_DESCRIPTION, name);
    }

    final BlueprintContext updatedContext =
        super.updateBlueprintContext(blueprintContext);

    return updatedContext;
  }

  // --- object basics --------------------------------------------------------

}
