/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42.app.config;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminCenter;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSpaceContextProvider;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSpacePageContextProvider;
import de.smartics.projectdoc.atlassian.confluence.macro.analyzer.XhtmlContentHandler;
import de.smartics.projectdoc.atlassian.confluence.persistence.ProjectdocDocumentService;
import de.smartics.projectdoc.atlassian.confluence.persistence.ProjectdocMarkerSupport;
import de.smartics.projectdoc.atlassian.confluence.tools.i18n.I18nService;
import de.smartics.projectdoc.atlassian.confluence.util.ProjectdocUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class ProjectdocComponentImports {
  @Bean
  public ProjectdocDocumentService projectdocDocumentService() {
    return importOsgiService(ProjectdocDocumentService.class);
  }

  @Bean
  public ProjectdocUser projectdocUser() {
    return importOsgiService(ProjectdocUser.class);
  }

  @Bean
  public I18nService i18nService() {
    return importOsgiService(I18nService.class);
  }

  @Bean
  public SpaceAdminCenter spaceAdminCenter() {
    return importOsgiService(SpaceAdminCenter.class);
  }

  @Bean
  public ProjectdocMarkerSupport projectdocMarkerSupport() {
    return importOsgiService(ProjectdocMarkerSupport.class);
  }

  @Bean
  public ContextProviderSupportService contextProviderSupportService() {
    return importOsgiService(ContextProviderSupportService.class);
  }

  @Bean
  public XhtmlContentHandler xhtmlContentHandler() {
    return importOsgiService(XhtmlContentHandler.class);
  }

  @Bean
  public ProjectDocSpaceContextProvider projectDocSpaceContextProvider() {
    return importOsgiService(ProjectDocSpaceContextProvider.class);
  }

  @Bean
  public ProjectDocSpacePageContextProvider projectDocSpacePageContextProvider() {
    return importOsgiService(ProjectDocSpacePageContextProvider.class);
  }
}
