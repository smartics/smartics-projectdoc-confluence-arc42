/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContextKeys;
import com.atlassian.confluence.util.i18n.I18NBean;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.BlueprintContextNames;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.page.partition.AbstractProjectDocSectionContextProvider;

import java.util.Objects;

import static de.smartics.projectdoc.atlassian.confluence.util.PageTitleReplacements.PARENT_PAGE_TITLE;

/**
 * Extends for injection to SWADs.
 */
public abstract class AbstractProjectDocSwadSectionContextProvider
    extends AbstractProjectDocSectionContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String TEMPLATE_LABEL = "swad-section";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected AbstractProjectDocSwadSectionContextProvider(
      final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected boolean matchesTemplateLabel(final String templateLabel) {
    return templateLabel == null || TEMPLATE_LABEL.equals(templateLabel);
  }

  @Override
  protected String calcShortKey(final BlueprintContext blueprintContext) {
    final Object contentTemplateKey =
        blueprintContext.get("contentTemplateKey");
    return Objects.toString(contentTemplateKey, "");
  }

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final I18NBean i18n = user.createI18n();
    if (isSwadSection()) {
      blueprintContext.put("templateLabel", "swad-section");
      final String key = calcShortKey(blueprintContext);
      final String name = i18n.getText("projectdoc.doctype." + key + ".name");
      final String shortDescription =
          i18n.getText("projectdoc.doctype." + key + ".short-description");
      blueprintContext.put("projectdoc_doctype_volume_name", name);
      blueprintContext.put(BlueprintContextNames.NAME, name);
      blueprintContext.put(BlueprintContextNames.SHORT_DESCRIPTION,
          shortDescription);
    }

    final BlueprintContext updatedContext =
        super.updateBlueprintContext(blueprintContext);
    final String title = (String) updatedContext.get(
        BlueprintContextKeys.CONTENT_PAGE_TITLE.key());
    if (null != title && title.endsWith(PARENT_PAGE_TITLE)) {
      updateTitle(updatedContext, i18n);
    }

    updateContextFinally(updatedContext);

    return updatedContext;
  }

  protected void updateTitle(final BlueprintContext blueprintContext,
      final I18NBean i18n) {
    final String updatedTitle;
    final String name =
        (String) blueprintContext.get(BlueprintContextNames.NAME);
    if (isSwadSection()) {
      updatedTitle =
          name + calcSwadSectionTitleExtension(blueprintContext, i18n);
    } else {
      updatedTitle = name;
    }
    blueprintContext.setTitle(updatedTitle);
  }

  protected String calcSwadSectionTitleExtension(
      final BlueprintContext blueprintContext, final I18NBean i18n) {
    // If we create a space templateLabel will be null.
    final Object templateLabel = blueprintContext.get("templateLabel");
    return templateLabel == null ?
           " - " + i18n.getText("projectdoc.volume.template.title") :
           (String) blueprintContext.get(BlueprintContextNames.NAME);
  }

  protected boolean isSwadSection() {
    return false;
  }

  // --- object basics --------------------------------------------------------

}
