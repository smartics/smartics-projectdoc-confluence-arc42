/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42.subspace;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.atlassian.confluence.ProjectDocContext;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.page.partition.AbstractProjectDocSectionContextProvider;

/**
 * Extends for injection to a subspace.
 */
public class SubspaceContextProvider
    extends AbstractProjectDocSectionContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String TEMPLATE_LABEL = "subspace-home";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public SubspaceContextProvider(final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    return super.updateBlueprintContext(blueprintContext);
  }

  @Override
  protected boolean matchesTemplateLabel(final String templateLabel) {
    return TEMPLATE_LABEL.equals(templateLabel);
  }

  @Override
  protected String calcAdjustedName(final ProjectDocContext blueprintContext,
      final String replacedName) {
    return replacedName;
  }

  // --- object basics --------------------------------------------------------

}
