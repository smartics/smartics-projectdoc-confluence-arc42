/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42.app.config;

import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.ProjectDocSwadContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionBuildingBlockViewContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionConceptsContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionConstraintsContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionContextContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionDeploymentViewContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionDesignDecisionsContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionGlossaryContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionIntroductionContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionOverviewContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionQualityScenariosContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionRuntimeViewContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionSolutionStrategyContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider.ProjectDocSwadSectionTechnicalRisksContextProvider;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.subspace.SubspaceContextProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ConfluenceComponentImports.class, ProjectdocComponentImports.class})
public class ProvidersConfig {
  @Bean
  public SubspaceContextProvider subspaceContextProvider(
      final ContextProviderSupportService support) {
    return new SubspaceContextProvider(support);
  }

//  @Bean
//  public ProjectDocSwadSectionContextProvider projectDocSwadSectionContextProvider(
//      final ContextProviderSupportService support) {
//    return new ProjectDocSwadSectionContextProvider(support);
//  }

  @Bean
  public ProjectDocSwadContextProvider projectDocSwadContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionIntroductionContextProvider projectDocSwadSectionIntroductionContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionIntroductionContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionConstraintsContextProvider ProjectDocSwadSectionConstraintsContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionConstraintsContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionContextContextProvider ProjectDocSwadSectionContextContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionContextContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionSolutionStrategyContextProvider ProjectDocSwadSectionSolutionStrategyContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionSolutionStrategyContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionBuildingBlockViewContextProvider ProjectDocSwadSectionBuildingBlockViewContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionBuildingBlockViewContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionRuntimeViewContextProvider ProjectDocSwadSectionRuntimeViewContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionRuntimeViewContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionDeploymentViewContextProvider ProjectDocSwadSectionDeploymentViewContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionDeploymentViewContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionConceptsContextProvider ProjectDocSwadSectionConceptsContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionConceptsContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionDesignDecisionsContextProvider ProjectDocSwadSectionDesignDecisionsContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionDesignDecisionsContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionQualityScenariosContextProvider ProjectDocSwadSectionQualityScenariosContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionQualityScenariosContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionTechnicalRisksContextProvider ProjectDocSwadSectionTechnicalRisksContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionTechnicalRisksContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionGlossaryContextProvider ProjectDocSwadSectionGlossaryContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionGlossaryContextProvider(support);
  }

  @Bean
  public ProjectDocSwadSectionOverviewContextProvider ProjectDocSwadSectionOverviewContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocSwadSectionOverviewContextProvider(support);
  }
}
