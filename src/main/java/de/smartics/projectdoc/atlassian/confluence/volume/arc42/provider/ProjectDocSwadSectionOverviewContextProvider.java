/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
package de.smartics.projectdoc.atlassian.confluence.volume.arc42.provider;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.volume.arc42.AbstractProjectDocSwadSectionContextProvider;

/**
 * Returns <code>arc42-swad-section-overview-template</code>
 */
public class ProjectDocSwadSectionOverviewContextProvider
    extends AbstractProjectDocSwadSectionContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocSwadSectionOverviewContextProvider(
      final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected String calcShortKey(final BlueprintContext blueprintContext) {
    return "arc42-swad-section-overview-template";
  }

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final BlueprintContext updatedContext =
        super.updateBlueprintContext(blueprintContext);

    //    final I18NBean i18n = user.createI18n();
    //    final String extension = calcSwadSectionTitleExtension
    //    (updatedContext);
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-introduction-template
    //        .name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-constraints-template
    //        .name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-context-template.name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype
    //        .arc42-swad-section-solution-strategy-template.name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype
    //        .arc42-swad-section-building-block-view-template.name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-runtime-view-template
    //        .name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-deployment-view-template
    //        .name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-concepts-template.name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype
    //        .arc42-swad-section-design-decisions-template.name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype
    //        .arc42-swad-section-quality-scenarios-template.name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-technical-risks-template
    //        .name");
    //    update(blueprintContext, i18n, extension,
    //        "projectdoc.doctype.arc42-swad-section-glossary-template.name");

    return updatedContext;
  }

  //  private void update(final BlueprintContext blueprintContext,
  //      final I18NBean i18n, final String extension, final String key) {
  //    final String page =
  //        i18n.getText(key) + (extension != null ? extension : "");
  //    blueprintContext.put(key + ".page", page);
  //  }

  // --- object basics --------------------------------------------------------

}
