/*
 * Copyright 2014-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This license applies only to the artifacts of this project.
 *
 * arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
 * using arc42 please visit their website!
 *
 * arc42
 *  Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
 *  http://arc42.com/
 */
require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
    "use strict";

    AJS.bind("blueprint.wizard-register.ready", function () {
        // unused right now
        function submitProjectdocSpace(e, state) {
            state.pageData.ContentPageTitle = state.pageData.name;
            return Confluence.SpaceBlueprint.CommonWizardBindings.submit(e, state);
        }
        // unused right now
        function preRenderProjectdocSpace(e, state) {
            state.soyRenderContext['atlToken'] = AJS.Meta.get('atl-token');
            state.soyRenderContext['showSpacePermission'] = false;
        }

        Confluence.Blueprint.setWizard('de.smartics.atlassian.confluence.smartics-projectdoc-confluence-arc42:projectdoc-space-blueprints-item-arc42', function (wizard) {
            wizard.on("pre-render.projectdoc-arc42-spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.preRender);
            wizard.on("post-render.projectdoc-arc42-spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.postRender);
            wizard.on("submit.projectdoc-arc42-spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.submit);
        });
    });
});