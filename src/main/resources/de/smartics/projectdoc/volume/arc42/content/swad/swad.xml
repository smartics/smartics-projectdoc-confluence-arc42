<!--

    Copyright 2014-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    This license applies only to the artifacts of this project.

    arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
    using arc42 please visit their website!

    arc42
     Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
     http://arc42.com/

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">swad</ac:parameter>
        <ac:parameter ac:name="override">false</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table class="confluenceTable">
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  <td class="confluenceTd"><at:var at:name="projectdoc_doctype_volume_shortDescription"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name"/></th>
                  <td class="confluenceTd"><at:var at:name="projectdoc_doctype_volume_name"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortName" /></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.shortName.placeholder" /></ac:placeholder></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.parent"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-transclusion-parent-property">
                      <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.audience"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">role</ac:parameter>
                      <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.subject" /></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                    <ac:parameter ac:name="doctype">subject</ac:parameter>
                    <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.subject" /></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.categories"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">category</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.categories"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-tag-list-macro">
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.flags" /></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.flags.placeholder" /></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.iteration"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-iteration">
                      <ac:parameter ac:name="value">facade</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sponsors"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">stakeholder,organization,person,role</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.sponsors"/></ac:parameter>
                      <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.sortKey.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th>enable-heading-numbers</th>
                  <td>true</td>
                  <td>hide</td>
                </tr>
                <tr>
                  <th>use-document-heading-number</th>
                  <td>false</td>
                  <td>hide</td>
                </tr>
                <tr>
                  <th>heading-number-start</th>
                  <td>1</td>
                  <td>hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>

  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.description"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.swad.description.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.summary"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.summary.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro
        ac:name="projectdoc-transclude-documents-macro">
        <ac:parameter ac:name="doctype">swad-section</ac:parameter>
        <ac:parameter ac:name="taget-heading-level">*</ac:parameter>
        <ac:parameter ac:name="space-key"><ri:space ri:space-key="@self" /></ac:parameter>
        <ac:parameter ac:name="tags">SWAD</ac:parameter>
        <ac:plain-text-body></ac:plain-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.notes"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.notes.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.references"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.references.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.resources"/></ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.resources.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>
