<!--

    Copyright 2014-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    This license applies only to the artifacts of this project.

    arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
    using arc42 please visit their website!

    arc42
     Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
     http://arc42.com/

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">dashboard</ac:parameter>
        <ac:parameter ac:name="hide">true</ac:parameter>
        <ac:parameter ac:name="override">true</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table class="confluenceTable">
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  <td class="confluenceTd"><at:i18n at:key="projectdoc.content.doctools.documentation-dashboard.short-description"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name"/></th>
                  <td class="confluenceTd"><at:i18n at:key="projectdoc.content.doctools.documentation-dashboard.name"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.parent"/></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-transclusion-parent-property">
                      <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.audience"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">role</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.audience"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.subject"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">subject</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.subject"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.categories"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">category</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.categories"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-tag-list-macro">
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
                      <ac:parameter ac:name="names">doctype-index, all</ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.iteration"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-iteration">
                      <ac:parameter ac:name="value">facade</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.sortKey.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <p><at:i18n at:key="projectdoc.documentation-dashboard.intro"/></p>
    </ac:layout-cell>
  </ac:layout-section>
  <ac:layout-section ac:type="three_equal">
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="borderColor">red</ac:parameter>
        <ac:parameter ac:name="borderWidth">3</ac:parameter>
        <ac:rich-text-body>
          <h2><at:i18n at:key="projectdoc.documentation-dashboard.faults.heading"/></h2>
          <p><at:i18n at:key="projectdoc.documentation-dashboard.faults.intro"/></p>
          <p>
            <ac:structured-macro ac:name="projectdoc-display-table">
              <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.doctype"/>, <at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
              <ac:parameter ac:name="where">macroName:projectdoc-box-fault*</ac:parameter>
              <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="borderColor">green</ac:parameter>
        <ac:parameter ac:name="borderWidth">3</ac:parameter>
        <ac:rich-text-body>
          <h2><at:i18n at:key="projectdoc.documentation-dashboard.feedback.heading"/></h2>
          <p><at:i18n at:key="projectdoc.documentation-dashboard.feedback.intro"/></p>
          <p>
            <ac:structured-macro ac:name="projectdoc-display-table">
              <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.doctype"/>,  <at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
              <ac:parameter ac:name="where">macroName:projectdoc-box-feedback*</ac:parameter>
              <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="borderColor">orange</ac:parameter>
        <ac:parameter ac:name="borderWidth">3</ac:parameter>
        <ac:rich-text-body>
          <h2><at:i18n at:key="projectdoc.documentation-dashboard.pending.heading"/></h2>
          <p><at:i18n at:key="projectdoc.documentation-dashboard.pending.intro"/></p>
          <p>
            <ac:structured-macro ac:name="projectdoc-display-table">
              <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.doctype"/>, <at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
              <ac:parameter ac:name="where">macroName:projectdoc-box-pending*</ac:parameter>
              <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>
