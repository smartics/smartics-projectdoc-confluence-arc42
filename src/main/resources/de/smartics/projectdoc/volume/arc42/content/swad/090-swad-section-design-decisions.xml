<!--

    Copyright 2014-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    This license applies only to the artifacts of this project.

    arc42 is owned by Dr. Peter Hruschka & Dr. Gernot Starke. For details on
    using arc42 please visit their website!

    arc42
     Copyright 2002-2015 Dr. Peter Hruschka & Dr. Gernot Starke
     http://arc42.com/

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">swad-section</ac:parameter>
        <ac:parameter ac:name="override">false</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table class="confluenceTable">
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  <td class="confluenceTd"><at:var at:name="projectdoc_doctype_common_shortDescription"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name"/></th>
                  <td class="confluenceTd"><at:var at:name="projectdoc_doctype_common_name"/></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.categories"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">category</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.categories"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-tag-list-macro">
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.iteration"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-iteration">
                      <ac:parameter ac:name="value">facade</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></th>
                  <td class="confluenceTd">090</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.docsubtype"/></th>
                  <td class="confluenceTd">swad-section-design-decisions</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th>enable-heading-numbers</th>
                  <td>true</td>
                  <td>hide</td>
                </tr>
                <tr>
                  <th>use-document-heading-number</th>
                  <td>true</td>
                  <td>hide</td>
                </tr>
                <tr>
                  <th>heading-number-start</th>
                  <td>9</td>
                  <td>hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.description"/></ac:parameter>
        <ac:parameter ac:name="numbering">false</ac:parameter>
        <ac:parameter ac:name="show-title">false</ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.description.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.summary"/></ac:parameter>
        <ac:parameter ac:name="numbering">false</ac:parameter>
        <ac:parameter ac:name="show-title">false</ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.summary.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.arc42-swad-section-design-decisions.listing"/></ac:parameter>
        <ac:parameter ac:name="tags">SWAD</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">architecture-decision</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.parent"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">false</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank-text"><at:i18n at:key="projectdoc.arc42.label.noDesignDecisionsSpecified"/></ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.section.children"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">section</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>

      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.notes"/></ac:parameter>
        <ac:parameter ac:name="required-permissions">write-access</ac:parameter>
        <ac:rich-text-body>
          <ac:placeholder><at:i18n at:key="projectdoc.doctype.common.notes.placeholder"/></ac:placeholder>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.references"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro
            ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="render-as-definition-list">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="projectdoc.doctype.common.title"/></ac:parameter>
            <ac:rich-text-body>
              <table class="wrapped">
                <colgroup>
                  <col />
                  <col />
                </colgroup>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="projectdoc.doctype.common.title"/></th>
                    <th><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  </tr>
                  <tr>
                    <td>
                      <br />
                    </td>
                    <td>
                      <br />
                    </td>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.resources"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro
            ac:name="projectdoc-tour-macro">
            <ac:parameter ac:name="render-as-definition-list">true</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="replace-title-with-name">true</ac:parameter>
            <ac:parameter ac:name="marker-column-property-name"><at:i18n at:key="projectdoc.doctype.common.title"/></ac:parameter>
            <ac:rich-text-body>
              <table class="wrapped">
                <colgroup>
                  <col />
                  <col />
                </colgroup>
                <tbody>
                  <tr>
                    <th><at:i18n at:key="projectdoc.doctype.common.title"/></th>
                    <th><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  </tr>
                  <tr>
                    <td>
                      <br />
                    </td>
                    <td>
                      <br />
                    </td>
                  </tr>
                </tbody>
              </table>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>