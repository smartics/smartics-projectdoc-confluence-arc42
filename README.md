projectdoc Add-on for arc42
===========================


## Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

[arc42](http://arc42.de/) ([en](http://arc42.org/)) contains a template for development, documentation and communication of
software architectures. This add-on provides blueprints to bring the arc42
Template to Confluence for easy use.

This add-on provides the core blueprints to create pages for

  * the arc42 Template
  * Blackboxes, Whiteboxes
  * Interfaces

Use the space blueprints of the [Software Development Doctypes](https://www.smartics.eu/confluence/display/PDAC1/Software+Development+Doctypes) to create spaces for your software projects.

## Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Core Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

This license applies only to the artifacts of this project.

[arc42](http://arc42.de/) is **owned by Dr. Peter Hruschka & Dr. Gernot Starke**. For details on using arc42 please visit [their website](http://arc42.de/) ([en](http://arc42.org/)) !


## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/projectdoc+Add-on+for+arc42)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-arc42)

This add-on uses templates from

  * [projectdoc Core Doctypes Add-on](https://www.smartics.eu/confluence/display/PDAC1/Doctypes#Doctypes-CoreDoctypes)
  * [projectdoc Doctypes for Software Development](https://www.smartics.eu/confluence/display/PDAC1/Software+Development+Doctypes)
